Get-ChildItem -Filter *.csv | % { Get-Content $_.Name | ConvertFrom-Csv -Delimiter ";" | ConvertTo-Csv -Delimiter "`t" -NoTypeInformation | Out-File ("{0}.tsv" -f $_.BaseName); }
