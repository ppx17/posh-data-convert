$Excel = New-Object -ComObject Excel.Application;
$Files = Get-ChildItem -Filter *.xlsx;

Foreach($File in $Files) {
    $Workbook = $excel.Workbooks.Open($File.FullName);

    Foreach($Sheet in $Workbook.sheets) {

        $Data = [System.Collections.ArrayList]::new();
        $LastRow = 0;
        $Row = $null;

        Foreach($Cell in $Sheet.UsedRange) {
            if($Cell.Row -ne $LastRow) {
                if($null -ne $Row) {
                    [void] $Data.Add($Row);
                }
                $LastRow = $Cell.Row;
                $Row = [System.Collections.ArrayList]::new();
            }

           [void]$Row.Add($Cell.Text);
        }

        [void]$Data.Add($Row);

        if($Data.Count -lt 1) {
            continue;
        }

        $Header = $Data[0];
        $Data.RemoveAt(0);

        $DataObjects = [System.Collections.ArrayList]::new();
        Foreach($Row in $Data) {
            $obj = New-Object PSObject;
            for($i=0; $i -lt $Header.Count; $i++) {
                $obj | Add-Member -MemberType NoteProperty -Name $Header[$i] -Value $Row[$i];
            }
            [void]$DataObjects.Add($obj);
        }

        $Output = ("{0}-{1}.tsv" -f $File.BaseName, $Sheet.Name);
        $DataObjects |  ConvertTo-Csv -Delimiter "`t" -NoTypeInformation | Out-File $Output;
    }
}
